import { Component, Output} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { DataapiService } from './services/dataapi.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  menuprincipal = [
    { nombre: 'Inicio', url: 'home' },
    { nombre: 'Acerca', url: 'acerca' }
  ];
  // estadisticasPorTipo:Array<any>;
  constructor(public route: ActivatedRoute, private dataapi: DataapiService){
    // this.estadisticasPorTipo= new Array<any>();
  }
  // Función que carga los primeros Pokemones y los agrega al servicio DataapiService
  traerListaInicial(){
    this.dataapi.consultarPokemones();
  }
  ngAfterViewInit(){
    //Llamado a la función de carga de Listado de pokemones
    this.traerListaInicial();
  }
  // ----------------
  seleccionarNavActivo(event){
    console.log('CambioSeleccion', event);
  }
}
