import { Component, OnInit } from '@angular/core';
import { DataapiService } from '../services/dataapi.service';
import * as CanvasJS from '.././canvasjs.min';
import { Router } from '@angular/router';


@Component({
  selector: 'app-estadisticas',
  templateUrl: './estadisticas.component.html',
  styleUrls: ['./estadisticas.component.css']
})
export class EstadisticasComponent implements OnInit {
  constructor(public dataapi: DataapiService, private router:Router) { }

  ngOnInit(): void {
    console.log("Estadisticas",this.dataapi.traerEstadisticas());
    let chartbar = new CanvasJS.Chart("chartContainerBar", {
    animationEnabled: true,
    exportEnabled: true,
    title: {
      text: "Estadísticas Barra Pokemón"
    },
    data: [{
        type: "column",
        click: (evt) => {
          this.dataapi.setFiltroPokemon(evt.dataPoint.label);
          this.router.navigate(["home"]);
        },
        dataPoints: this.dataapi.traerEstadisticas()
      }]
    });
    chartbar.render();
    // -------------Char Pie
    let chartpie = new CanvasJS.Chart("chartContainerPie", {
    animationEnabled: true,
    exportEnabled: true,
    title: {
      text: "Estadísticas Pastel Pokemón"
    },
    data: [{
      type: "pie",
      showInLegend: true,
      toolTipContent: "<b>{label}</b>: ${y} (#percent%)",
      indexLabel: "{label} - #percent%",
        click: (evt) => {
          this.dataapi.setFiltroPokemon(evt.dataPoint.label);
          this.router.navigate(["home"]);
        },
        dataPoints: this.dataapi.traerEstadisticas()
      }]
    });
    chartpie.render();
  }
}
