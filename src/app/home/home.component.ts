import { Component, OnInit} from '@angular/core';
import { DataapiService } from '../services/dataapi.service';
import { Router } from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  coloresAsignados:Array<any>;
  detallesPokemon:[];
  filtrogeneral:{};
  constructor(public dataapi: DataapiService, private router:Router, private modalService: NgbModal) {
    console.log("Constuctor Home");
    this.filtrogeneral={tipo:this.dataapi.filtroPokemon};
    this.coloresAsignados=new Array<any>();
  }
  crearEstaditicasPorTipo():void{

    //Crea las estadística por tipo de Pokemones
    this.dataapi.inicializarEstadisticas();
    // --------------
    let tiposPorPokemon=[];
    //----------
    console.log(this.dataapi.traerPokemones());
    this.dataapi.traerPokemones().forEach(pokemon => {
      if(pokemon.types){
        for (let i = 0; i < pokemon.types.length; i++) {
          const tipoP = pokemon.types[i];
          let indexEncontrado=this.dataapi.traerEstadisticas().findIndex(item=> item.label === tipoP.type.name);
          if(indexEncontrado!=-1){
            // console.log("indexEncontrado",indexEncontrado);
          this.dataapi.modificarItemEstadisticas(indexEncontrado);
          }
          else{
            this.dataapi.agregarItemEstadisticas({label:tipoP.type.name,y:1});
          }
        }
      }
    });
    // --------------
    this.router.navigate(["estadisticas"]);
  }
  traerListaSiguiente(){
    console.log("this.dataapi.traerOffset() + this.dataapi.traerLimite()",this.dataapi.traerOffset() + this.dataapi.traerLimite());

    this.dataapi.setOffset(this.dataapi.traerOffset() + this.dataapi.traerLimite());
    this.dataapi.consultarPokemones();
  }

  // ----------------
  seleccionarNavActivo(event){
    console.log('CambioSeleccion', event);
  }
  ngOnInit():void {
    console.log("Aca ngInit Home");
    console.log("Colores",this.coloresAsignados);

  }
  habilidadesPokemon(habilidades:[any]):string {
    var respuesta="";
    if (habilidades!=undefined) {
      for (let i = 0; i < habilidades.length; i++) {
        const habilidad = habilidades[i].ability;
        respuesta+=habilidad.name+" ";
      }
    }
    return respuesta;
  }
  imagenPokemon(imagenes:any):string {
    var imagen="";
    if (imagenes!=undefined) {
      imagen = imagenes.other.dream_world.front_default;
    }
    return imagen;
  }
  onScroll(){
    this.traerListaSiguiente();
  }
  contador(cont:number){
    return new Array(cont);
  }
  asignarColorPorTipo(tipos:any):string{
    if(tipos.length==1){
      let indexEncontrado=this.coloresAsignados.findIndex(item=> item.tipo1 === tipos[0].type.name && item.tipo2 === "" );
      if(indexEncontrado!=-1){
        return this.coloresAsignados[indexEncontrado].color;
      }
      else{
        let color=this.getRandomColor();
        this.coloresAsignados.push({
          tipo1:tipos[0].type.name,
          tipo2:"",
          color: color
        });
        return color;
      }
    }
    else if(tipos.length==2){
      let indexEncontrado=this.coloresAsignados.findIndex(item=> item.tipo1 === tipos[0].type.name && item.tipo2 === tipos[1].type.name );
      if(indexEncontrado!=-1){
        return this.coloresAsignados[indexEncontrado].color;
      }
      else{
        let color=this.getRandomColor();
        this.coloresAsignados.push({
          tipo1:tipos[0].type.name,
          tipo2:tipos[1].type.name,
          color: color
        });
        return color;
      }
    }
    // console.log("tipos",tipos);
    // console.log("Asiganods",this.coloresAsignados);

  }
  reiniciarFiltro(){
    this.filtrogeneral="";
    this.dataapi.setFiltroPokemon("");
  }
  getRandomColor() {
    var color = Math.floor(0x1000000 * Math.random()).toString(16);
    return '#' + ('000000' + color).slice(-6);
  }
}
