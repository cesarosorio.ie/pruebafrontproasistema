import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AcercaComponent } from './acerca/acerca.component';
import { DataapiService } from './services/dataapi.service';
import {HttpClientModule} from '@angular/common/http';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { EstadisticasComponent } from './estadisticas/estadisticas.component';
import { FiltropokemonPipe } from './services/filtropokemon.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AcercaComponent,
    EstadisticasComponent,
    FiltropokemonPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    InfiniteScrollModule,
    NgxSkeletonLoaderModule
  ],
  providers: [
    DataapiService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
