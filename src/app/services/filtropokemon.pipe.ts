import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'filtrop',
    pure: false
})
export class FiltropokemonPipe implements PipeTransform {
    transform(items: any[], filter: any): any {
        if (!items || !filter) {
            return items;
        }
        return items.filter(item => {
          let respuesta;
          if(filter.tipo.length!=0){
            respuesta=item.types.findIndex(item=> item.type.name === filter.tipo) !== -1;
          }
          else{
            respuesta=1;
          }
          return respuesta
        });
    }
}
