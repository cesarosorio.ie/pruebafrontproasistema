import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class DataapiService {

  listado:Array<any>;
  headers:{};
  urlApi:string;
  estadisticasPorTipo:Array<any>;
  loading:false;
  limite:number;
  offset:number;
  filtroPokemon:string;
  constructor(private http: HttpClient) {
    this.estadisticasPorTipo=new Array<any>();
    this.listado=new Array<any>();
    this.urlApi="https://pokeapi.co/api/v2/";
    this.limite=30;
    this.offset=0;
    this.filtroPokemon="";
    console.log("DATAAPI INICIADO");
    // -----------------------------------------
    this.headers=new HttpHeaders()
    .set('Content-Type', 'text/xml')
    .append('Access-Control-Allow-Methods', 'GET, POST')
    .append('Access-Control-Allow-Origin', '*')
    .append('Access-Control-Allow-Headers', "Access-Control-Allow-Headers, Access-Control-Allow-Origin, Access-Control-Request-Method");
  }
  traerLista(){
    return new Promise<any>((resolve, reject) => {
      this.http.get(this.urlApi+"pokemon/?limit="+this.limite+"&offset="+this.offset,{headers:this.headers})
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  };
  consultarPokemones(){
    // -------------------------
    for (let i = 1; i <= this.limite; i++) {
      this.setLoading(true);
        this.http.get(this.urlApi+"pokemon/"+(this.offset+i),{headers:this.headers})
        .subscribe(
          res => {
            this.listado.push(res);
            this.setLoading(false);
          },
          err => {
            console.log(err);
          }
        );
      }
    // --------------------------
  };
  inicializarEstadisticas(){
    this.estadisticasPorTipo=[];
    console.log("Vacia",this.estadisticasPorTipo);
  }
  traerEstadisticas() {
    return this.estadisticasPorTipo;
  }
  agregarItemEstadisticas(item:any){
    this.estadisticasPorTipo.push(item);
  }
  modificarItemEstadisticas(index:number){
    this.estadisticasPorTipo[index].y++;
  }
  setLoading(estado){
    this.loading=estado;
  }
  setListado(datos){
    this.listado=datos;
  }
  setOffset(cantidad){
    this.offset=cantidad;
  }
  traerPokemones(){
    return this.listado;
  }
  traerLimite():number{
    return this.limite;
  }
  traerOffset():number{
    return this.offset;
  }
  agregarPokemon(dato){
    this.listado.push(dato);
  }
  traerLitimePokemon(){
    return this.limite;
  }
  setFiltroPokemon(dato){
    this.filtroPokemon=dato;
  }
  traerFiltroPokemon(){
    return this.filtroPokemon;
  }
}
