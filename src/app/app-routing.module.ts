import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// Componentes para Routes
import { HomeComponent } from './home/home.component';
import { AcercaComponent } from './acerca/acerca.component';
import { EstadisticasComponent } from './estadisticas/estadisticas.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'acerca',
    component: AcercaComponent
  },
  {
    path: 'estadisticas',
    component: EstadisticasComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
