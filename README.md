# Frontendtest

Este proyecto fue generado con la [Angular CLI](https://github.com/angular/angular-cli) version: 10.2.0.
Hace parte de las prueba técnica para Proasistemas

## Arquitectura

El proyecto está compuesto por los siguientes componentes:
* App
  * Home
  * Acerca
  * Estadisticas

Además se compone de los siguientes servicios
* servicios/dataapi.service
* servicios/filtropokemon.pipe

Correr `ng build` para construir el proyecto.Este será ubicado en la carpeta `dist/`.

## Requisitos y dependencias

Para la ejecución del proyecto es necesario instalar los siguientes paquetes:
* ng-bootstrap `ng add @ng-bootstrap/ng-bootstrap`
* ng-infinite-scroll `npm install --save ngx-infinite-scroll`, para efectuar la carga a través del scroll.
* ng-skeleton-loader `npm install ngx-skeleton-loader`, para los efectos de carga
* canvasjs `npm install canvasjs `, para graficar las estadísticas.

## Ejecutar el proyecto

Luego de clonar el repositorio, debe instalar sus dependencias:
`npm install`
Luego puede correr con `ng serve`. Explorar en `http://localhost:4200/`.
